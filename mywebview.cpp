#include "mywebview.h"
#include <QDebug>

MyWebView::MyWebView(QWidget *parent) :
    QWebView(parent)
{
    connect( this, SIGNAL(loadFinished(bool)), this, SLOT(hackerSlot(bool)) );
}

// @Override
void MyWebView::setUrl(const QUrl &url)
{
    setVisible( false );
    QWebView::setUrl( url );
}

// ugly hacker solution
void MyWebView::hackerSlot(bool ok)
{
    setVisible( true );
    if (!ok)
    {
        emit parsedLoadFinished( false );
    }
    QString parsedHtml = "<h4>" + url().toString() + "</h4>";
    parsedHtml.append( parser.parseElements( page()->mainFrame() ) );
    blockSignals( true ); //necessary?
    setHtml( parsedHtml );
    blockSignals( false );
    emit parsedLoadFinished( true );
}

void MyWebView::setParser( const WebElementParser &p )
{
    parser = p;
}
