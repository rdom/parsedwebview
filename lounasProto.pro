#-------------------------------------------------
#
# Project created by QtCreator 2014-09-08T09:37:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network webkitwidgets

TARGET = lounasProto
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mywebview.cpp

HEADERS  += mainwindow.h \
    mywebview.h

FORMS    += mainwindow.ui
