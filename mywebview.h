#ifndef MYWEBVIEW_H
#define MYWEBVIEW_H

#include <QWebView>
#include <QWebElementCollection>
#include <QWebFrame>

/* TODO: add descendantRule as attribute,
 * i.e. should query be "E F" (descendant) or " E > F" (child)
 */
class WebElementParser
{
public:
    typedef enum {
        // TODO: findChild?
        findFirst,
        findAll
    }FIND_TYPE;
    WebElementParser()
    {
        selectorQuery = "*";
        type = findFirst;
    }
    QString getSelectorQuery() const
    {
        return selectorQuery;
    }
    FIND_TYPE getFindType() const
    {
        return type;
    }
    void setSelectorQuery( QString syntax )
    {
        selectorQuery = syntax;
    }
    void setFindType( FIND_TYPE findType )
    {
        type = findType;
    }
    void addSubElementParser( const WebElementParser &sub )
    {
        subElementParsers.append( sub );
    }
    void addSubElementParser( const QString &subRuleSyntax, FIND_TYPE type = findFirst )
    {
        WebElementParser subParser;
        subParser.setSelectorQuery( subRuleSyntax );
        subParser.setFindType( type );
        subElementParsers.append( subParser );
    }
    QString parseElements( const QWebFrame* mainFrame ) const
    {
        QList<QWebElement> list;
        if ( type == findAll )
        {
            list = mainFrame->findAllElements( selectorQuery ).toList();
        } else
        {
            list.append( mainFrame->findFirstElement( selectorQuery ) );
        }

        if ( list.isEmpty() )
        {
            return "";
        }

        // "1st round" of parsing is a special case :P
        if ( subElementParsers.isEmpty() )
        {
            QString parsedHtml = "";
            foreach( QWebElement e, list )
            {
                parsedHtml.append( e.toOuterXml() );
            }
            return parsedHtml;
        }

        return parseElements( list );
    }
private:
    QString selectorQuery; //Standard CSS2 selector syntax
    QList<WebElementParser> subElementParsers; // pointers?
    FIND_TYPE type;

    QString parseElements( const QList<QWebElement> &elements ) const
    {
        QString parsedHtml = "";
        QList<QWebElement> parsedElements;

        if ( subElementParsers.isEmpty() )
        {
            qDebug() << "Parsing with" << selectorQuery << "no subparsers";
            if ( type == findFirst )
            {
                qDebug() << "findFirst";
                foreach( QWebElement e, elements )
                {
                    parsedElements.append( e.findFirst( selectorQuery ) );
                }
            } else
            {
                qDebug() << "findAll";
                foreach( QWebElement e, elements )
                {
                    parsedElements.append( e.findAll( selectorQuery ).toList() );
                }
            }
            foreach( QWebElement p, parsedElements )
            {
                parsedHtml.append( p.toOuterXml() );
            }
        }
        else
        {
            qDebug() << "My rule is" << selectorQuery << "I has subparsers";
            QString tag = elements[0].tagName();
            foreach( QWebElement e, elements )
            {
                QString parsedElementHtml = "";
                parsedElementHtml.append("<"+tag+">");
                foreach( WebElementParser sub, subElementParsers )
                {
                    qDebug() << "My subParser has rule:" << sub.getSelectorQuery();
                    QList<QWebElement> tmp;
                    tmp.append( e );
                    parsedElementHtml.append( sub.parseElements( tmp ) );
                }
                parsedElementHtml.append("</"+tag+">");
                //qDebug() << "Subparser parsed:" << parsedElementHtml;
                parsedHtml.append( parsedElementHtml );
            }
        }
        return parsedHtml;
    }
};

class MyWebView : public QWebView
{
    Q_OBJECT
private:
    WebElementParser parser;
public:
    explicit MyWebView(QWidget *parent = 0);
    void setParser( const WebElementParser &p );
    void setUrl(const QUrl &url);
signals:
    void parsedLoadFinished( bool ok );
private slots:
    void hackerSlot( bool ok );
};

#endif // MYWEBVIEW_H
