#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUrl>
#include <QWebView>
#include <QWebFrame>
#include <QWebElementCollection>
#include <QDebug>
#include "mywebview.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    Ui::MainWindow *ui;
    QMap<QUrl, WebElementParser> urlsToParse;
    QList<MyWebView*> views;
    int sitesFetched;
private slots:
    void connectButtonClicked();
    void replyFinished( bool ok );
};

#endif // MAINWINDOW_H
