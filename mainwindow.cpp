#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect( ui->connectButton, SIGNAL( clicked() ), this, SLOT( connectButtonClicked() ) );
    sitesFetched = 0;

    QUrl sonaattiUrl = QUrl("http://www.sonaatti.fi");
    WebElementParser sonaattiParser;
    sonaattiParser.setSelectorQuery( "DIV.ruuat" );
    sonaattiParser.setFindType( WebElementParser::findAll );
    sonaattiParser.addSubElementParser( "H3" );
    sonaattiParser.addSubElementParser( "SPAN.toimipiste" );
    sonaattiParser.addSubElementParser( "SPAN.paiva" );
    sonaattiParser.addSubElementParser( "P.ruokatiedot" );
    urlsToParse.insert(  sonaattiUrl, sonaattiParser );

    QUrl haraldUrl = QUrl("http://www.ravintolaharald.fi/ruoka--ja-juomalistat/lounas");
    WebElementParser haraldParser;
    haraldParser.setSelectorQuery("TABLE[id=\"lounaslistaTable\"]");
    urlsToParse.insert( haraldUrl, haraldParser );

    QUrl oldBrickUrl = QUrl("http://jyy.fi/ruokalistat/");
    WebElementParser oldBrickParser;
    oldBrickParser.setSelectorQuery("DIV.menu-day");
    oldBrickParser.setFindType( WebElementParser::findAll );
    urlsToParse.insert( oldBrickUrl, oldBrickParser );

    int i = 0;
    foreach( QUrl url, urlsToParse.keys() )
    {
        MyWebView *view = new MyWebView( this );
        view->setParser( urlsToParse.value( url ) );
        connect( view, SIGNAL(parsedLoadFinished(bool)), this, SLOT(replyFinished(bool)) );
        ui->gridLayout->addWidget( view, 0, i );
        views.append( view );
        i++;
    }

    /* "DEMO"
    WebElementParser parser;
    parser.setSelectorQuery( "UL" );
    parser.setFindType( WebElementParser::findAll );
    WebElementParser sub;
    sub.setSelectorQuery( "LI" );
    sub.setFindType( WebElementParser::findAll );
    sub.addSubElementParser( "A" );
    parser.addSubElementParser( sub );
    myWebView->setParser( parser );*/
}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connectButtonClicked()
{
    sitesFetched = 0;
    ui->connectButton->setEnabled(false);
    int i = 0;
    foreach( QUrl url, urlsToParse.keys() )
    {
        views.at( i )->setUrl( url );
        i++;
    }
}

void MainWindow::replyFinished( bool ok )
{
    if (!ok)
    {
        qDebug() << "Load failed";
    }
    sitesFetched++;
    if ( sitesFetched >= views.count() )
    {
        ui->connectButton->setEnabled( true );
    }
}
